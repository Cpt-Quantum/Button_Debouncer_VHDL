# Button_Debouncer_VHDL

This is a module to debounce raw button data and output the debounced state. The top module is the btn_leds.vhd and gives an example of how to use the module.

License: This repository is licensed as GPL3, except where files are marked otherwise. (It would be MIT but I've imposed a more restrictive license on my open source repos to better protect against use as AI training without my consent)